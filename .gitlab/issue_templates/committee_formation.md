# Security Culture Committee Formation
This issue tracks the formation of a new cohort of the [Security Culture Committee](https://about.gitlab.com/handbook/engineering/security/security-culture.html).

This is meant to be used by an outgoing committee to run the process of having a new committee nominated and formed.

## Action Items

- [ ] [Prepare the Google form nomination survey](https://about.gitlab.com/handbook/engineering/security/security-culture.html#prepare-the-nomination-forms)
- [ ] [Make an announcement on Slack and send out an email with the survey](https://about.gitlab.com/handbook/engineering/security/security-culture.html#announce-that-nominations-are-open)
- [ ] [Collect the results and contact the nominees to make sure they accept. Ask backup nominees if needed.](https://about.gitlab.com/handbook/engineering/security/security-culture.html#form-the-new-committee)
- [ ] Announce the new committee members in the `#security-department` Slack channel
- [ ] [Schedule a handover meeting](https://about.gitlab.com/handbook/engineering/security/security-culture.html#conduct-a-handover-meeting)
- [ ] Add any changes to the nomination template + process to the security culture committee handbook section

## Resources

* Nomination Form templates can be found in the [Nominations folder in Google Docs](https://drive.google.com/drive/folders/1ZLsvJnQD2jWZonALkDQuIURkh60kTIZH?usp=sharing)

## Boilerplate

### Nomination email boilerplate
> Hello security team members!
>
> Nominations are open for the next security culture committee.
>
> Please nominate one team member from each sub-department. We'll take the top three nominations in each group and the top two nominated participants across the board for a total of 5.
>
> Please complete the survey by (insert date here): (insert nomination form link here)
>
> Learn more about this committee in the handbook.
>
> Questions? Ask us in slack at #security-culture!

### Slack DM boilerplate

Asking if someone accepts the nomination:

> hey NAME! You received the most nominations from your peers to join the security culture committee for your department. There’s no pressure to do this, and you can decline for any reason or no reason at all. I’m happy to link you any documentation or answer any questions you might have before you accept or decline the nomination. You’re also more than welcome to take some time to think about it and consider the nomination before responding. When you’ve made your decision, please let me know — do you accept the nomination to join the security culture committee?

In the event that they do accept the nomination:

> Thank you for your response and congratulations. I’m really excited to see what wonderful efforts and initiatives you and the rest of the next security committee will take on! Once we’ve reached out to everyone and gotten the committee fully formed, we will announce the new committee in Slack and then schedule a ‘handover’ meeting with the current committee to answer any questions and/or otherwise help y’all get started — from there, it’ll be up to y’all to decide what you want to do! More details soon.
