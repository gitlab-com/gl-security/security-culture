## Current situation

Describe the current situation and mark the impacted values below. 

### Values impacted
- [ ] Collaboration
- [ ] Results
- [ ] Efficiency
- [ ] Diversity, Inclusion & Belonging
- [ ] Iteration
- [ ] Transparency

## Proposed change

Describe the proposed change and how it will positively influence the impacted values.
Give an expected timeline for the change to happen.

## Expected outcome

Describe how the proposed change will have influence within the security department. 
